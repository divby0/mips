# MIPS Projekt
# Authors: 6094572 &&  2630150
# Version: 0.3
#|0.1
#|0.0 INIT
# TAYLOR APPROXIMATION of e^x and ln(x)  at  0
#

.data
  menuString:             .asciiz "  \n#=#========= Menu ============# \n1 | exponential approximation |\n2 | logarithmic approximation |\n3 | Interval approximation    |\n0 | EXIT                      | \n#=#========= END =============# \n  \nPlease Enter the number according to the Action you want to execute:\n  "
  expoInputX:             .asciiz "You selected exponential approximation.\nPlease enter x for approximation of e^x:\n    x = "
  lnInputX:               .asciiz "You selected logarithmic approximation.\nPlease enter x for approximation of ln(x):\n   x = "
  inputIterations:        .asciiz "Please enter the number of iterations / the precision.\n iterations = "
  lowerboundaryInterval:  .asciiz "You selected Interval approximation.\n [ X_low,X_upper ) element of R\nPlease enter lower boundary for the Interval:\n  X_low = "
  upperBoundaryInterval:  .asciiz "Please enter upper boundary for the Interval:\n X_upper = "
  IntervalSelection:      .asciiz "Please enter the size of Intervals for the Interval:\n n = "
  intervalCurrent:        .asciiz "\n Interval value beeing tested: "
  output:                 .asciiz "The result is:\n "
  Exponential:            .asciiz "-------Expomnential Approximation------\n "
  Ln:                     .asciiz "-----------Ln Approximation-----------\n "
  exitString:             .asciiz "Program is shutting down...\n"
  lnErrorText:            .asciiz "Logarithmic functions take x > 0 only. Nice try, though...\n"
  lineBreak:              .asciiz "\n"

  # those two are two differents variables because recursiveBorder can be everything,
  # but we need the floatOne always.
  # It is pure coincidence that they have the same value
  recursiveBorder:            .float 1.0
  floatOne:                 .float 1.0
  floatZero:                .float 0.0

  # we could also not store the euler number,
  # but calculate it live by setting x to 1 and approximating e^x.
  # We decided against it, because it is a typicall cpu vs memory tradeoff.
  # We went with less computation time but one more variable to save.
  euler:                .float 2.7182818285

.text

main:
  # show menu and prompt for integer
  #and $s4, $s5, $s4
  #s4     1/0    is Exponentialapprox in Interval  / in NOT Exponentialapprox in Interval
  #s5     1/0     is Ln approximation in Interval   / is NOT Ln approx
  beq $s4, 1, backlogQuadcodeExponential   # is s4 True ; s4 is True if it is an Intervalrequest
  beq $s5, 1, backlogQuadcodeLN            # is LN the reason why exit was done return to Interval part after ln approx was called to continue
  la $a0, menuString              # Load menuString to print
  jal promptIntegerHelper         # Print Menu and get Selection back
  # branch to the needed subprogram or exit
  beq $v0, 1, expoParameterSetup        # Branch to direct e^x subprogramm
  beq $v0, 2, logParameterSetup         # Branch to direct ln programm
  beq $v0, 3, intervalParameterSetup    # Branch to Interval subprogram


  # li $a0, 10
  # li.s $f12, 3.1
  #
  # jal factorial
  #
  # move $a0, $v0
  # li $v0, 1
  # syscall

  j exit                         # Call exit Subprogramm to cleanup and exit

#Setup Parameters for Interval berechnung
intervalParameterSetup:
    # Input for Interval X  element R
    # Prompt for INT  input  of
    la $a0, lowerboundaryInterval  # Load Input for Lower Interval string
    jal promptFloatHelper          # get float input & print String
    mov.s $f12, $f0                # f12 = X_lowerbound

    la $a0, upperBoundaryInterval  #Load string for Upper Interval input
    jal promptFloatHelper          # Print String and get Float Input
    mov.s $f13, $f0                # f13 = X_upper      f 13 Holds upper Limit

    la $a0, IntervalSelection      # Load string fot Interval size
    jal promptFloatHelper          # Print String and get Float Input ( size of Interval)
    mov.s $f11, $f0                 # f11 = n ( size of Interval )

    # Move Parameters of functioncall to return Parameters : Standardized Size of Interval = f0 ; current lowerbound = f1; current upperbound = f2
    mov.s $f0,$f11                # Size of Interval to fitting Return Parameter
    mov.s $f1,$f12                # current Lowerbound to fiiting Retunr Parameter
    mov.s $f2,$f13                # current Upperbound to fiiting Return Parameter

    j intervalLoop


#Subprogramm of Intervalcalculation ; Loop until Lowerbound > Upperbound
intervalLoop:
  # Get Results from  return Registers to function parameters / to "any" kind of registers to work with
  # Here used f10 - f13 to keep some standartization
  mov.s $f11,$f0    # Size of Interval                                      f0 -> f11
  mov.s $f12,$f1    # Lowerbound from return Register to working registers  f1 -> f12
  mov.s $f13,$f2    # Upperbound from Return Register to working registers  f2 -> f13


  c.lt.s $f13,$f12  # Is Loop finished ;  if current Lowerbound > Upperbound
  bc1t finishedIntervalLoop # If Loop finished jump to Loop Cleanup


  li $v0, 4
  la $a0, intervalCurrent # Print Information about current Calculation
  syscall

  li $v0, 2         # Print current Lowerbound to show value X that is used to calculate E^x && ln(x)
  syscall

  li $v0, 4
  la $a0, lineBreak    # Print Linebreak
  syscall
  #Save f11 - f13 because registers are overridden in subprogram
  mtc1 $zero, $f9          #f9 == $zero ; We need a value to add float because $zero crashes my QtSpim for some reason
  add.s $f5,$f11,$f9      #save Interval size because f11 might be overridden
  add.s $f6,$f12,$f9      #save lower bound because f 12 is overridden
  add.s $f7,$f13,$f9      #save upper bound because f 13 is overridden

  li $v0, 4
  la $a0, Exponential # Print Header of Exponential to show that Following Result is from Exponential Approximation
  syscall

  #exponential Approximation
  # f12 is already x  ( vorrausdenkend gearbeitet ;) )
  # 16 iterations best accuracy
  addi $s4,$zero,1        #set s4 to activate jump back into subprogram from main if it is Exponential approximation
  addi $a0, $zero, 16     #Set

  jal expoApprox      # Calculate E^x with X = current lowerbound

backlogQuadcodeExponential:      # Jump back into subprogram  when E^x finishes with  "j main"

  add.s $f12,$f6,$f9      # reinstate f12 as lowerbound
  add.s $f11,$f5,$f9      # reinstate f11 as size of Interval
  add.s $f13,$f7,$f9      # reinstate f13 as upperbound

  # Because E^x <= 1 has prints the Result in a higher function that isn't accessed here we have to print the result seperatly here
  c.lt.s $f12, $f4        # is Lowerbound smaller than 1
  bc1t printIfOne
  c.eq.s $f12, $f4        # is lowerbound equal to 1
  bc1t printIfOne
  j skip                  # LOwerbound > 1  ==> Result is printed by Exponentialapprox directly
printIfOne:               # Lowerbound <= 1 Print Result here seperate
  li $v0, 4
  la $a0, output          # Print "The Result is:"
  syscall
  # output approximation
  li $v0, 2
  mov.s $f12, $f21       # Print Exponentialapprox result, float number in f21 ( moved to f12)
  syscall


skip:                   # Result was printed already just skip to here
  li $v0, 4
  la $a0, lineBreak       # Print Line break
  syscall


  addi $s4,$zero,-1       # Remove Flag that Exponential approximation is / was calculated

  mtc1 $zero, $f9         #f9 == $zero ; We need a value to add float because $zero crashes my QtSpim for some reason

  add.s $f11,$f5,$f9      # reinstate size of Interval f11
  add.s $f12,$f6,$f9      # reinstate lowerbound into f12
  add.s $f13,$f7,$f9      # reinstate Upperbound of Interval f 13

  #Logarithmic Approximation

  li $v0, 4
  la $a0, Ln # Print Ln header that shows the following Result is grom a ln calculation
  syscall

  cvt.w.s $f8,$f12
  mfc1 $a1, $f8               # convert lowerbound to Integer and save it into Parameter ( as X ) for Ln(x) calculation

  # set iterations
  addi $s5,$zero,1    # Set Flag for ln approximation, if subprogramm needs to jump back into THIS part of Code, like above  just for Ln

  addi $a0,$zero,20   # space efficiency 20 Iterations

  jal lnApprox        # Calculate Ln(x) approximation

backlogQuadcodeLN:    # Jump back if LN appriximation exits to main

  mtc1 $zero, $f9         #f9 == $zero ; We need a value to add float because $zero crashes my QtSpim for some reason
  add.s $f11,$f5,$f9      # reinstate size of Interval f11
  add.s $f12,$f6,$f9      # reinstate lowerbound into f12
  add.s $f13,$f7,$f9      # reinstate Upperbound of Interval f 13

  c.lt.s $f12, $f4          # Because ln(x) < 1 has prints the Result in a higher function that isn't accessed here we have to print the result seperatly here
  bc1t printIfLessThanOneLN  # Print results

  j skipLn                  # Results have been Printed
printIfLessThanOneLN:       # Print results of ln approximation
  li $v0, 4
  la $a0, output            # The Result is :
  syscall
  # output approximation
  li $v0, 2
  mov.s $f12, $f21          # Print Results
  syscall

skipLn:
  li $v0, 4
  la $a0, lineBreak         # print \n
  syscall

  addi $s5,$zero,-1        # Remove Flag that Ln approx was / is made

  add.s $f12,$f6,$f9      # reinstate f12 as lowerbound
  add.s $f11,$f5,$f9      # reinstate f12
  add.s $f13,$f7,$f9      # reinstate f12

  add.s $f12, $f11, $f12    #Increase lowerborder to get next value to test && to not have an never ending loop

  #write results in return registers
  mov.s $f2,$f13 # upperbound to upperbound return reg
  mov.s $f0,$f11 # size back to return reg
  mov.s $f1,$f12 # lowerbound to return reg of lowerbound

  j intervalLoop

finishedIntervalLoop:
  # clear up loop
  mtc1 $zero, $f9       #Reset f9
  j main                # Jump back Programm finished

lnError:
  la $a0, lnErrorText
  li $v0, 4
  syscall

  j main

logParameterSetup:
  # prompt for x
  la $a0, lnInputX
  jal promptFloatHelper
  mov.s $f12, $f0       # f12 = x

  lwc1 $f13, floatZero
  c.le.s $f12, $f13
  bc1t lnError

  # prompt for iteration-count
  la $a0, inputIterations
  jal promptIntegerHelper
  move $a0, $v0       # a0 = iterations

  jal lnApprox

  # output output-string
  li $v0, 4
  la $a0, output
  syscall

  # output approximation
  li $v0, 2
  mov.s $f12, $f21
  syscall

  j main

expoParameterSetup:
  # prompt for x
  la $a0, expoInputX
  jal promptFloatHelper
  mov.s $f12, $f0       # f12 = x

  # prompt for iteration-count
  la $a0, inputIterations
  jal promptIntegerHelper
  move $a0, $v0       # a0 = iterations
  bgt $a0, 16, fixIterationsE

  jal expoApprox

  # output output-string
  li $v0, 4
  la $a0, output
  syscall

  # output approximation
  li $v0, 2
  mov.s $f12, $f21
  syscall

  j main

# limit iteration count because with more iterations problems in taylor polynomials arise
fixIterationsE:
  li $a0, 16

  jal expoApprox
  # output output-string
  li $v0, 4
  la $a0, output
  syscall

  # output approximation
  li $v0, 2
  mov.s $f12, $f21
  syscall

  j main

# ===== calculates approximated e^x ===== #
# input:
#   $a0 := iteration-count
#   $f12 := x
# output:
#   $f21 := approximated e^x
expoApprox:
  # check if we have to use recursive expoApprox call
  lwc1 $f4, recursiveBorder

  c.lt.s $f4, $f12
  bc1t recursiveExpoApprox

  # if we are here, we can go without recursive expoApprox call
  li $s0, 0  # setup loop variable
  li.s $f21, 0.0 # setup taylor sum
  j expoLoop

# ===== calculates approximated ln(x) ===== #
# input:
#   $a0 := iteration-count
#   $f12 := x
# output:
#   $f21 := approximated ln(x)
lnApprox:
  # check if we have to use recursive expoApprox call
  lwc1 $f4, recursiveBorder

  c.lt.s $f4, $f12
  bc1t recursiveLnApprox

  # if we are here, we can go without recursive expoApprox call
  li $s0, 1  # setup loop variable
  li.s $f21, 0.0 # setup taylor sum
  j lnLoop

recursiveLnApprox:
  mov.s $f26, $f12    # a = x
  li $t0, 0           # steps = 0
  j recursiveLnApproxLoop

recursiveLnApproxLoop:
  li.s $f25, 1.0
  c.lt.s $f26, $f25
  bc1t recursiveLnApproxEnd

  li.s $f25, 2.0
  div.s $f26, $f26, $f25   # a = a/2
  addi $t0, $t0, 1        # steps++

  j recursiveLnApproxLoop

recursiveLnApproxEnd:
  # convert steps in float and store in $f26
  mtc1 $t0, $f28
  cvt.s.w $f28, $f28

  # calc steps*0.69314718056
  li.s $f27, 0.69314718056
  mul.s $f31, $f28, $f27

  cvt.w.s $f13, $f12
  cvt.s.w $f13, $f13

  mov.s $f12, $f26

  jal lnApprox
  # approximation is in f21 now

  add.s $f21, $f21, $f31

  # output output-string
  li $v0, 4
  la $a0, output
  syscall

  # output approximation
  li $v0, 2
  mov.s $f12, $f21
  syscall

  j main

# no stack needed in "recursive" call because recursion is always 1 level deep at max
recursiveExpoApprox:
  # substract one from our x
  lwc1 $f22, floatOne
  sub.s $f22, $f12, $f22

  # convert to int (conversion = cut off = floor)
  cvt.w.s $f22, $f22
  mfc1 $s2, $f22

  addi $s2, $s2, 1      # we want ceil(x) instead of floor(x)

  # save $f12 and $a0 away
  move $s3, $a0
  mov.s $f23, $f12

  # calculate e to the power of ceil(x)
  lwc1 $f12, euler
  move $a0, $s2

  jal power
  mov.s $f24, $f0     # e to the power of ceil(x) is in $f0 now, save it away

  # load $f12 and $a0 again
  move $a0, $s3
  mov.s $f12, $f23

  # recursive call with only x-n as parameter, not x
  mtc1 $s2, $f22
  cvt.s.w $f22, $f22
  sub.s $f12, $f12, $f22

  jal expoApprox

  mul.s $f21, $f21, $f24

  # output output-string
  li $v0, 4
  la $a0, output
  syscall

  # output approximation
  li $v0, 2
  mov.s $f12, $f21
  syscall

  j main

lnLoop:
  beq $s0, $a0, outputResult
  addi $sp, $sp, -8             # move stack-pointer
  sw $a0, 0($sp)                # save $a0 to stack
  sw $ra, 4($sp)

  # save f12 (x) away, so we can call power subprogram
  addi $sp, $sp, -4
  swc1 $f12, 0($sp)

  li.s $f12, -1.0
  addi $a0, $s0, 1

  jal power
  # -1 to the power of (i+1) is in $f0 now
  mov.s $f25, $f0 # save this result away

  # load f12 (x) again
  lwc1 $f12, 0($sp)
  addi $sp, $sp, 4

  # setup power parameters
  li.s $f26, -1.0
  add.s $f12, $f12, $f26
  move $a0, $s0

  jal power
  # (x-1) to the power of i is in $f0 now

  li.s $f26, 1.0
  add.s $f12, $f12, $f26

  # calculate power(x-1,i) divided by i
  mtc1 $s0, $f26
  cvt.s.w $f26, $f26
  div.s $f22, $f0, $f26

  # f22 contains one taylor monom now
  mul.s $f22, $f22, $f25
  add.s $f21, $f21, $f22

  lw $a0, 0($sp)                # save $a0 to stack
  lw $ra, 4($sp)
  addi $sp, $sp, 8             # move stack-pointer
  addi $s0, $s0, 1              # count loop variable up

  j lnLoop

expoLoop:
  beq $s0, $a0, outputResult    # condition to break out of loop
  addi $sp, $sp, -8             # move stack-pointer
  sw $a0, 0($sp)                # save $a0 to stack
  sw $ra, 4($sp)                # save $ra to stack
  move $a0, $s0
  # call power function (since parameters are already setup)
  jal power
  # $f0 contains the calculated power now
  mov.s $f20 $f0            # save our power ($f0)
  # call factorial function (since parameters are already setup)
  jal factorial
  # $v0 contains the calculated factorial now
  mtc1 $v0, $f0
  cvt.s.w $f0, $f0
  div.s $f0, $f20, $f0      # f0 = power/factorial
  add.s $f21, $f21, $f0     # f21 = f21 + power/factorial

  lw $a0, 0($sp)            # load $a0 from stack
  lw $ra, 4($sp)            # load $ra from stack
  addi $sp, $sp, 8          # move stack-pointer

  addi $s0, $s0, 1
  j expoLoop

outputResult:
  jr $ra

# ===== prompts the user to input float ===== #
# input:
#   $f12 := base
#   $a0 := exponent
# output:
#   $f0 := base to the power of exponent
power:
  # check if exponent is 0
  beq $a0, 0, ret1F0

  # setup loop variable and product for power
  li $t0, 1
  mov.s $f0, $f12
  j powerLoop

powerLoop:
  beq $t0, $a0, powerEnd    # loop break condition

  mul.s $f0, $f0, $f12      # actual multiplication for power
  addi $t0, $t0, 1          # counting loop variable up

  j powerLoop

powerEnd:
  jr $ra

# ===== prompts the user to input float ===== #
# input:
#   $a0 := factorial input
# output:
#   $v0 := $a0 factorial
factorial:
  # check if $a0 == 1 | $a0 == 0
  beq $a0, 1, ret1V0
  beq $a0, 0, ret1V0

  # setup loop variable and product for factorial
  li $t0, 2
  li $v0, 1
  j factorialLoop

factorialLoop:
  mult $v0, $t0               # multiplication for power
  mflo $v0

  beq $t0, $a0, factorialEnd  # loop break condition

  addi $t0, $t0, 1               # counting up loop variable

  j factorialLoop

factorialEnd:
  jr $ra

ret1V0:
  li $v0, 1
  jr $ra

ret1F0:
  li.s $f0, 1.0
  jr $ra

exit:
  li $v0, 4
  la $a0, exitString
  syscall

  li $v0, 10  # load 10 for syscall exit
  syscall     # actually exit

# ===== prompts the user to input integer ===== #
# input:
#   $a0 := promptStringAdress
# output:
#   $v0 := user inputed int
promptIntegerHelper:
  li $v0, 4           # load 4 for syscall output string
  syscall             # actually print lnInputX

  li $v0, 5           # load 5 for syscall integer input
  syscall             # actually read the integer
  jr $ra

# ===== prompts the user to input float ===== #
# input:
#   $a0 := promptStringAdress
# output:
#   $f0 := user inputed float
promptFloatHelper:
  li $v0, 4           # load 4 for syscall output string
  syscall             # actually print lnInputX

  li $v0, 6           # load 6 for syscall float input
  syscall             # actually read the float
  jr $ra
