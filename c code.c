#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/* TODOS:
    e^x can only be approximated with iteration count <= 13
        - because factorial is larger than FLOAT_MAX
        - division is negative somehow
    e^x can only be approximated with x < 6
*/

int factorial(int);
float pot(float, int);
float exponential(float, int);
int factorial(int);
float ln(float, int);
float ln_recursive(float, int);
float power(float, int);

int main(void)
{
    int error = 0;
    int input = 6;
    int iterations;
    float startValue;
    float x;

    while (input != 0)
    {
        printf("#########  Menu ######## \n");
        printf("--+-----------------------+------------------\n");
        printf("1 | exponential approximation |\n");
        printf("2 | logarithmic approximation |\n");
        printf("--+-----------------------+------------------\n");
        printf("0 | EXIT                  |\n");
        printf("Please Enter the number according to the Action you want to execute:");

        scanf("%d", &input);
        switch (input)
        {
        case 1:
            printf("You selected exponential approximation.\n");
            printf("Please enter x for approximation of e^x:\n x = ");
            scanf("%f", &startValue);
            printf("Please enter the number of iterations / the precision:\n");
            scanf("%d", &iterations);
            x = exponential(startValue, iterations);
            printf("Taylorapprox: %.6e\n", x);
            printf("Result of C math library %.6e\n", expf(startValue));
            break;
        case 2:
            printf("You selected logarithmic aproximation\n");
            printf("Please enter x for approximation of ln(x):\n x = ");
            scanf("%f", &startValue);
            printf("Please enter the number of iterations / the precision:\n");
            scanf("%d", &iterations);
            x = ln(startValue, iterations);
            printf("Taylorapprox: %.6e\n", x);
            printf("Result of C math library %.6e\n", logf(startValue));
            break;
        case 0:
            printf("Program is shutting down.\n");
            break;
        default:
            printf("Please select a real Input\n");
            break;
        }
    }
    return 1;
}

float exponential(float startValue, int iteration)
{
    if(startValue > 1){
        int n = ceil(startValue-1);
        float power_helper = power(2.7182818285, n);
        float recursive_expo = exponential(startValue-n, iteration);
        return power_helper*recursive_expo;
    }else{
        float result;
        float helper, helper2;
        if(iteration > 16 ){
            iteration = 16;
        }
        for (int j = 0; j < iteration; j++)
        {
            // printf("Term:  %d\n", j);
            helper = pot(startValue, j);
            helper2 = helper / factorial(j);
            result = helper2 + result;
            // printf("result  %f\n", result);
        }
        return result;
    }
}

// returns ln with `x` as argument and approximated with `iterations` iterations.
float ln(float x, int iterations){
    if(x > 1){
        float a = x; // using x=a*(2^b) here with b=1
        int steps = 0;
        while(a > 1){
            a = a/2;
            steps++;
        }

        return ln(a, iterations) + steps*0.69314718056;
    }else{
        float sum = 0;

        for(int i = 1; i<iterations; i++){
            sum += power(-1, i+1)*(power(x-1, i)/i);
        }

        return sum;
    }
}

// returns ln with `x` as argument and approximated with `iterations` iterations.
float ln_recursive(float x, int iterations){
    if(x > 2){
        float a = x/2; // using x=a*(2^b) here with b=1
        return ln_recursive(a, iterations) + 0.69314718056;
    }
    else{
        float sum = 0;

        for(int i = 1; i<iterations; i++){
            sum += power(-1, i+1)*(power(x-1, i)/i);
        }

        return sum;
    }
}


// generate b to the e-th power for integer e values
float power(float b, int e){
    float product = 1;

    for(int i = 0; i < e; i++){   // e times multiply b with itself
        product *= b;
    }

    return product;
}

float pot(float start, int exponent)
{
    float result = 1;
    for (int i = 0; i < exponent; i++)
    {
        result = start * result;
    }
    // printf("pot: %f\n", result);
    return result;
}

int factorial(int i)
{
    if(i==0 || i==1)
        return 1;
    float product = 1;
    for (int j = 2; j <= i; j++){
        product*=j;
    }
    // printf("fac: %f\n", product);
    return product;
}
